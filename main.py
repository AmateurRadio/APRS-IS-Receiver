import sys
import aprslib
import datetime
from csv import DictWriter
from os.path import exists
from os.path import basename
from os import remove
import boto3
from botocore.exceptions import ClientError
import logging

call_sign = 'USDOTSDC'
passwd = "-1"
host = "noam.aprs2.net"
port = 14580
skip_login = False
file_name = False
max_lines = 1000 + 1
field_names = ['Datetime', 'ID', 'Symbol', 'Lat', 'Lon', 'Alt', 'Speed', 'Course', 'Raw']
bucket = "dev.sdc.dot.gov.jeffs.bucket.of.crap"

'''
Filters
http://www.aprs-is.net/javAPRSFilter.aspx
p = Position packets
o = Objects
i = Items
m = Message
q = Query
s = Status
t = Telemetry
u = User-defined
n = NWS format messages and objects
w = Weather
r/lat/lon/dist
'''
f_type = ' t/p '
f_not_type = ' -t/oimqstunw '
f_volpe_loc = 'r/42.3655656/-71.1541139/100'
filter_str = f_type + f_not_type + f_volpe_loc


def upload_file(file_name_, bucket_, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name_: File to upload
    :param bucket_: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    object_name = "APRS-IS/" + file_name_
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = basename(file_name_)

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name_, bucket_, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def lat_lon_filter(lat, lon):
    # only packets inside a box around the US
    far_n = 71.344028
    far_s = 18.910833
    far_e = -66.947028
    far_w = -168.118889
    if far_n >= lat >= far_s:
        if far_w <= lon <= far_e:
            return True
        else:
            return False
    else:
        return False


def append_dict_as_row(file_name_, dict_of_elem, field_names_):
    # Open file in append mode
    with open(file_name_, 'a+', encoding="utf-8", newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=field_names_)
        # Add dictionary as new row in the csv
        dict_writer.writerow(dict_of_elem)


def callback(packet):
    # print(packet)
    global file_name, field_names, max_lines
    # packet must have lat lon info
    if 'latitude' in packet and 'longitude' in packet:
        # lat lon must be inside the box
        if lat_lon_filter(packet['latitude'], packet['longitude']):
            pkt = {}
            dt = datetime.datetime.now()
            # if it's the first time running, set the file name
            if not file_name:
                file_name = 'APRS-IS Data {}.csv'.format(dt.strftime("%Y-%m-%d_%H.%M.%S"))

            # build the packet
            pkt['Datetime'] = str(dt)
            pkt['ID'] = packet['from']
            pkt['Lat'] = str(round(packet['latitude'], 6))
            pkt['Lon'] = str(round(packet['longitude'], 6))
            pkt['Raw'] = packet['raw']
            # packets may not have all fields, if not make it empty string
            if 'symbol_table' in packet and 'symbol' in packet:
                pkt['Symbol'] = str(packet['symbol_table'] + packet['symbol'])
            else:
                pkt['Symbol'] = ''
            if 'altitude' in packet:
                pkt['Alt'] = str(int(packet['altitude']))
            else:
                pkt['Alt'] = ''
            if 'speed' in packet:
                pkt['Speed'] = str(int(packet['speed']))
            else:
                pkt['Speed'] = ''
            if 'course' in packet:
                pkt['Course'] = str(int(packet['course']))
            else:
                pkt['Course'] = ''

            # have we done this before?
            if exists(file_name):
                num_lines = sum(1 for line in open(file_name, encoding='utf8'))
                # have we done it x number of times? If so, make new file.
                if num_lines >= max_lines:
                    # upload to s3
                    uploaded = upload_file(file_name, bucket)
                    if uploaded:
                        remove(file_name)
                    else:
                        sys.exit()
                    # make new file
                    print("{:<28} {:<10} {:<12} {:<12} {:<8} {:<8} {:<8} {:}".format(
                        'Datetime',
                        'ID',
                        'Lat',
                        'Long',
                        'Altitude',
                        'Speed',
                        'Course',
                        'Raw Packet'
                    ))
                    file_name = 'APRS-IS Data {}.csv'.format(dt.strftime("%Y-%m-%d_%H.%M.%S"))
                    file_header = {'Datetime': 'Datetime', 'ID': 'ID', 'Symbol': 'Symbol', 'Lat': 'Lat', 'Lon': 'Lon',
                                   'Alt': 'Alt', 'Speed': 'Speed', 'Course': 'Course', 'Raw': 'Raw'}
                    append_dict_as_row(file_name, file_header, field_names)
            else:
                file_name = 'APRS-IS Data {}.csv'.format(dt.strftime("%Y-%m-%d_%H.%M.%S"))
                file_header = {'Datetime': 'Datetime', 'ID': 'ID', 'Symbol': 'Symbol', 'Lat': 'Lat', 'Lon': 'Lon',
                               'Alt': 'Alt', 'Speed': 'Speed', 'Course': 'Course', 'Raw': 'Raw'}
                append_dict_as_row(file_name, file_header, field_names)

            # Open file in append mode and save the packet data
            append_dict_as_row(file_name, pkt, field_names)

            # Print the packet data
            print("{:<28} {:<10} {:<12} {:<12} {:<8} {:<8} {:<8} {:}".format(
                pkt['Datetime'],
                pkt['ID'],
                pkt['Lat'],
                pkt['Lon'],
                pkt['Alt'],
                pkt['Speed'],
                pkt['Course'],
                pkt['Raw']
            ))


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)  # level=10
    # Print the header
    print("{:<28} {:<10} {:<12} {:<12} {:<8} {:<8} {:<8} {:}".format(
        'Datetime',
        'ID',
        'Lat',
        'Long',
        'Altitude',
        'Speed',
        'Course',
        'Raw Packet'
    ))
    AIS = aprslib.IS(call_sign, passwd, host, port, skip_login)
    AIS.set_filter(filter_str)
    AIS.connect()
    AIS.consumer(callback, raw=False)
